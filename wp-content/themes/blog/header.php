<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<link href="<?php bloginfo('template_directory'); ?>/config/style.css" rel="stylesheet">
</head>

<body <?php body_class(); ?>>
	
	<nav class="menu">
		
		<div class="logo">
			<img src="<?php bloginfo('template_directory'); ?>/config/img/insight.png" class="img-responsive">
		</div>
		
		
		<ul>
			<li>	<a href="#">Empresas</a>	</li>
			<li>	<a href="#">Serviços</a>	</li>
			<li>	<a href="#">Clientes e parceiros</a>	</li>
			<li>	<a href="#">Greensight</a>	</li>
			<li>	<a href="#">Processo Seletivo</a>	</li>
			<li>	<a href="#">Contato</a>	</li>
			
		</ul>
		
		<div class="clear"></div>
	</nav>




	

