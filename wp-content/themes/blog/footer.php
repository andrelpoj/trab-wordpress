<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->
		<footer id="rodape">
			<!--Footer acima da linha amarela-->
			<div class="container">
				<div class="insightTabela">
					<div class="quote">
						<p id="quote">"O diferencial da <span class="blue-text">Insight</span> está no comportamento empreendedor, crítico e analítico de nossos colaboradores que têm o compromisso com a responsabilidade social de suas ações."</p>
					</div>
				</div> 
				<div class="insight-icons">
					<img class="facebook-logo" src="<?php bloginfo('template_directory'); ?>/config/img/facebook.png" class="img-responsive">
					<img class="twitter-logo" src="<?php bloginfo('template_directory'); ?>/config/img/twitter.png" class="img-responsive">
					<img class="youtube-logo" src="<?php bloginfo('template_directory'); ?>/config/img/youtube_gray.png" class="img-responsive">
				</div>
			</div>
		
			<div class="clear"></div>
			
			<!--Linha amarela-->
			<div id="yellow-margin"></div>
			
			<div class="container">
				<div class="row text-center">
					<div class="insight-logo ">
						<a href= "http://www.insightjunior.com.br"><img class="insight-branca" src="<?php bloginfo('template_directory'); ?>/config/img/marcaBranca.png" class="img-responsive"></a>
					
					
					</div>
					
					<div id="footer-down-meio" >
						<p style="font-size: 13px;">Endereço: Av. Pasteur n° 250, Pavilhão Nilton Campos - IP <br> Telefone: 55 21 3938-5337</p>
						<span id="font-size-9">Copyright &copy; 2015 Insight Consultoria, todos os direitos reservados.</span>
					</div>
					
					<div class="footer-down-final">
						<p id="font-size-13" class="vertical-center">Desenvolvimento:</p>
						<a href="http://injunior.com.br"><img class="in-branca" src="<?php bloginfo('template_directory'); ?>/config/img/logo_IN_Junior_branca.png" class="img-responsive"></a>
					</div>
				</div>
			
			</div>


	</footer>
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
