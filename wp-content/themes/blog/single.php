<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
	<section class=" single ">
		  <?php the_post(); ?>
		  
		  <?php 
		  
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail'); ?>
        <?php if ($image):?>
            <div class="foto-conteudo" style="background-image: url(<?php echo $image[0]; ?>)"></div>
        <?php endif ?>
		  
		<div id = "texto-conteudo">
		  <h2><?php the_title(); ?></h2>
		  <p><?php echo the_content(); ?></p>
		</div>
	</section>

<?php get_footer(); ?>
