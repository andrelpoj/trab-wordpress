<?php /*Template Name: Blog
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
	<!--<section id="imgfundo">-->
	<!--	<div class="configTexto">-->
	<!--		<h1>Blog</h1>-->
	<!--		<p>-</p>-->
	<!--	</div>-->
	<!--</section>-->

	<section id="postsBlog">
		<div class="container">
		<?php query_posts("showposts=3&paged=$paged"); ?>
		    <?php if(have_posts()): ?>
            <?php while(have_posts()): ?>
            <?php the_post(); ?>
            
			<div class="box-conteudo">
				
				
				<?php 
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail'); ?>
	            <?php if ($image):?>
	                <div class="foto-conteudo" style="background-image: url(<?php echo $image[0]; ?>)"></div>
	            <?php endif ?>
					
				
				<div class="texto-conteudo">
					
					<h2><?php the_title(); ?></h2>
					<p><?php echo the_content(); ?></p>
					<a class="btn" href="<?php the_permalink();?>">Leia</a>
				</div>
			</div>
			
			<?php endwhile ?>
	            <?php endif ?> 
	     </div>    
		</section>
	
<?php get_footer(); ?>
